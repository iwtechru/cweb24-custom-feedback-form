<?php

defined('ABSPATH') or die("No script kiddies please!");

if (!function_exists('wp_get_current_user')) {
    include_once(ABSPATH . "wp-admin/includes/plugin.php");
}
if (!class_exists('cw_fw16')) {

    class cw_fw16 {

        public $plugindir;
        public $themedir;
        public $registered_posts;
        public $options;
        public $prefix;
        public $lang;
        public $plugin_folder;
        public $ajax_func;
        public $version;
        public $fields;
        public $plugin_uri;

        public static function init() {
            $class = __CLASS__;
            new $class;
        }

        public function __construct($pluginname = false, $settings = false) {
            $this->version = 1.5;
            $this->settings = array('access' => 10);
            $this->fields = array();
            $this->setlocale();
            if (($settings)) {
                foreach ($settings as $k => $v) {
                    $this->settings[$k] = $v;
                }
            }
            require_once(ABSPATH . 'wp-includes/pluggable.php');
            $this->cpt = array();
            $this->plugindir = dirname(__FILE__) . '';
          //  $this->plugin_uri=preg_replace('','',$this->plugindir);
            $tmp = explode('/', $this->plugindir);
            $tmp = array_reverse($tmp);
            $this->plugin_folder = $tmp[0];
            $this->plugindir = dirname(__FILE__) . '/';

            $this->plugin_uri=plugin_dir_url(__FILE__);//$this->plugin_folder.'/';
            $this->themedir = get_template_directory() . '/' . $this->prefix;
            if ($pluginname) {
                $this->pluginname = $pluginname;
            } else {
                $this->pluginname = 'My plugin';
            }
            $this->prefix = $this->makeprefix();
            add_action('init', array($this, 'init_cpt'));
            $this->initscripts();
            if (isset($_POST[$this->simplify()])) {
                $postvars = $_POST[$this->simplify()];
                if (isset($postvars['name']) && method_exists($this, 'action_' . $postvars['name']) && wp_verify_nonce($postvars['nonce'], $postvars['name'])) {
                    add_action('init', array($this, 'action_' . $postvars['name']));
                }
            }
            if ($this->get_option('custom_fields')) { // подключаем в админке произвольные поля для определенных записей
                add_action('add_meta_boxes', array($this, 'init_fields'), 10, 2);
                add_action('save_post', array($this, 'be_save_custom_fields'));
            }
            if ($this->get_option('attachments')) {
                add_filter('attachments_default_instance', '__return_false');
                add_action('attachments_register', array($this, 'init_attachments'));
            }
            $this->init_cronevents();
        }

        public function dbg($data) {
            if (!$this->settings['debug']) {
                return;
            } else {
                //$fname_ = debug_backtrace();
                //$fname = $fname_[1]['file'] . ':' . $fname_[1]['line'] . ' ';
                $fname = '';
                $logpath = dirname(__FILE__) . '/'.$this->prefix.'.log';
                file_put_contents($logpath, "\n" . date('Y-m-d H:i:s') . " " . $fname, FILE_APPEND);
                if (!is_string($data)) {
                    $data = print_r($data, true);
                }
                file_put_contents($logpath, $data, FILE_APPEND);
            }
        }

        public function init_cpt() {
            $posttypes = $this->get_option('cpt');
            if (!$posttypes) {
                return;
            }
            $tax = $this->get_option('taxonomies');
            if($tax){
            foreach ($tax as $posttype => $taxonomies) {
                foreach ($taxonomies as $label => $taxonomy) {
                    (register_taxonomy($label, $posttype, $taxonomy));
                }
            }}
            foreach ($posttypes as $k => $pt) {
               (register_post_type(sanitize_title($k), $pt));
            }
        }

        public function makeprefix() {
            //         $this->dbg('функция makeprefix');
            $tmp = explode(' ', $this->pluginname);
            $out='';
            foreach ($tmp as $v) {
                $out.=$v[0];
            }
            return 'cw_' . strtolower($out);
        }

        public function protectform($protect) {
            return wp_create_nonce($protect);
        }

        public function get_option($name) {
            $option = @unserialize(get_option($this->prefix . $this->simplify($name)));
            if (is_array($option)) {
                return $option;
            }
            $option = get_option($this->prefix . $this->simplify($name));
            return $option;
        }

        public function delete_option($option, $key = false) { // функция удаления группы настроек или части массива
            if ($key) {
                $options = $this->get_option($option);
                unset($options[$key]);
                update_option($this->prefix . $option, serialize($options));
            } else {
                delete_option($this->prefix . $option);
            }
        }

        public function add_attachments($type, $params) {
            // Создаем и выводим прикрепленные файлы
// Важно! Необходим плагин Attachments
            $default_args = array(//Значения по умолчанию
                'pid' => $pid,
                'notsingle' => false,
                'title' => __('Прикрепите файлы')
                );
            if (is_string($params)) {
                $params_['title'] = $params;
                $params = array();
                $params = $params_;
            }
            foreach ($params as $k => $v) {
                $default_args[$k] = $v;
            }

            $key = $this->sanitize($default_args['title']);
            if ($default_args['notsingle']) { // прикрепляем ко всем подобным записям notsingle==true
                $key = $key . $type;
                $this->update_option('attachments', $key, array(
                    'title' => $default_args['title'],
                    'post_type' => $type,
                    'notsingle' => $default_args['notsingle'],
                    'params' => $default_args['params']
                    ));
            } else { // прикрепляем только к данной конкретной записи notsingle==false
                $key = $key . '__' . $default_args['pid'];
                $this->update_option('attachments', $key, array(
                    'title' => $default_args['title'],
                    'post_type' => $type,
                    'notsingle' => $default_args['notsingle'],
                    'post_id' => $default_args['pid'],
                    ));
            }
        }

        public function get_attachments($pid, $key) {
            $files = json_decode(get_post_meta($pid, 'attachments', true), true);
            $key = str_replace('-', '_', $this->simplify($key)) . get_post_type($pid);
            return ($files[$key]);
        }

        public function get_thumb_src($pid, $args1 = 'full') {
// возвращает ссылку на миниатюру записи или картинку. По умолчанию - исходное загруженное изображение,
// либо через второй параметр передаем массив: array('width'=>ширина, 'height'=>высотка, 'crop'=>кадрирование (true/false));
            if (is_array($args1)) {
                $this->update_option('thumbs', $args1['width'] . $args1['height'] . $args1['crop'], array(
                    'width' => $args1['width'],
                    'height' => $args1['height'],
                    'crop' => $args1['crop']
                    )
                );
                $args = $args1['width'] . $args1['height'] . $args1['crop'];
            } else {
                $args = $args1;
            }
            switch (get_post_type($pid)) {
                case 'attachment':
                $att = wp_get_attachment_url($pid, $args);
                return $att;
                break;
                default:
                $att = wp_get_attachment_image_src(get_post_thumbnail_id($pid), $args);
                return $att[0];
                break;
            }
        }

        public function init_fields($post_type, $post) {
            $fields = $this->get_option('custom_fields');
            foreach ($fields as $key => $box) {
                if ($box['notsingle'] && $box['post_type'] == $post_type) {
                    add_meta_box(
                        $this->sanitize('custom fields post type ' . $key), __($box['label']), array($this, 'be_custom_field'), $post_type, 'normal', 'low', array('name' => $key, 'value' => $box)
                        );
                }
                if (!$box['notsingle'] && $post->ID == $box['post_id']) {
                    add_meta_box(
                        $this->sanitize('custom fields post type ' . $key), __($box['label']), array($this, 'be_custom_field'), $post_type, 'normal', 'low', array('name' => $key, 'value' => $box)
                        );
                }
            }
        }

        public function be_custom_field($post, $args) {
            $args['args']['field_value'] = htmlspecialchars(get_post_meta($post->ID, $args['args']['name'], true));
            $args['args']['field_name'] = 'cw[form][' . $args['args']['name'] . ']';
            $this->include_block('cf/' . $args['args']['value']['variant'], $args['args']);
        }

        public function init_attachments($attachments) {
            $att = $this->get_option('attachments');
            foreach ($att as $key => $value) {
                $fields = array(
                    array(
                        'name' => 'title', // unique field name
                        'type' => 'text', // registered field type
                        'label' => __('Title', 'attachments'), // label to display
                        'default' => 'title',
                        ),
                    array(
                        'name' => 'caption', // unique field name
                        'type' => 'textarea', // registered field type
                        'label' => __('Caption', 'attachments'), // label to display
                        'default' => 'caption', // default value upon selection
                        ),
                    );
                $args = array(// title of the meta box (string)
                    'label' => $value['title'],
                    // all post types to utilize (string|array)
//    'post_type' => array('post', 'page'),
                    'post_id' => array('post_id'),
                    // meta box position (string) (normal, side or advanced)
                    'position' => 'normal',
                    // meta box priority (string) (high, default, low, core)
                    'priority' => 'high',
                    // allowed file type(s) (array) (image|video|text|audio|application)
                    'filetype' => null, // no filetype limit
// include a note within the meta box (string)
                    'note' => _('Attach files here!'),
                    // by default new Attachments will be appended to the list
// but you can have then prepend if you set this to false
                    'append' => true,
                    // text for 'Attach' button in meta box (string)
                    'button_text' => __('Attach Files', 'attachments'),
                    // text for modal 'Attach' button (string)
                    'modal_text' => __('Attach', 'attachments'),
                    // which tab should be the default in the modal (string) (browse|upload)
                    'router' => 'browse',
                    // whether Attachments should set 'Uploaded to' (if not already set)
                    'post_parent' => false,
                    // fields array
                    'fields' => $fields,
                    );
if ($value['params']) {
    foreach ($value['params'] as $k => $v) {
        $args[$k] = $v;
    }
}
if (!$value['notsingle']) {

    if ((is_admin() && $_GET['post'] == $value['post_id'] && $_GET['action'] == 'edit') || (!is_admin())) {


        ($attachments->register($key, $args));
    }
} else {
                    //$args['post_type']
    $args['post_type'] = $value['post_type'];
    ($attachments->register($key, $args));
}
}
}

public function be_save_custom_fields($pid) {
    $fields = ($_POST['cw']['form']);
    foreach ($fields as $k => $v) {
                //$v = str_replace('"', '&quot;', $v);
        $v = htmlspecialchars_decode($v);
        (check_admin_referer('_' . $k, '_' . $k));
        update_post_meta($pid, $k, $v);
    }
}

public function include_block($file, $params = false) {
    if (file_exists(dirname(__FILE__) . '/blocks/' . $file . '.php')) {
        require(dirname(__FILE__) . '/blocks/' . $file . '.php');
    } else {
        echo (dirname(__FILE__) . '/blocks/' . $file . '.php');
    }
}

public function update_option($name, $value, $value1 = false) {
    if (!$value1) {
        update_option($this->prefix . $this->simplify($name), $value);
    } else {
        $options = $this->get_option($name);
        $options[$value] = $value1;
        update_option($this->prefix . $name, serialize($options));
    }
}

public function add_setting($name, $type = 'text', $array = false) {
    $this->options[$name] = array(
        'name' => (string) $name,
        'type' => (string) $type,
        'data_type' => $array,
        'value' => $this->get_option($name),
        'sysname' => $this->prefix . $this->simplify($name)
        );
    add_action('admin_init', array(&$this, 'register_settings'));
}

public function register_settings() {
    foreach ($this->options as $key => $value) {
        register_setting($this->prefix, $value['sysname']);
    }
}

public function sanitize($value) {
    return $this->simplify($value);
}

public function simplify($value = false) {
    $out = sanitize_title($value);
//  return 'crc_' . crc32($this->pluginname . $name);

    if (strlen($out) > 20) {
        $out = substr($out, 0, 9) . crc32($value);
    } else {

    }
    if (strlen($out) < 2) {
        $out = $this->prefix;
    }
    $out = str_replace('-', '_', $out);
    return $out;
}

public function settings_form() {
    $out.='<form method="post" action="options.php">';
    $out.='<table class="form-table">';
    ob_start();
    settings_fields($this->prefix);
    do_settings_sections($this->prefix);
    $out.=ob_get_clean();
    foreach ($this->options as $key => $value) {
        if (in_array($value['type'], array('timestamp', 'hidden'))) {

        } else {
            $out.='<tr><td>';
            if ($value['data_type']['name']) {
                $out.=_($value['data_type']['name']);
            } else {
                $out.=$key;
            }
            $out.='</td><td>';
        }
        $out.= $this->makefield($value['sysname'], $value['type'], $value);
        if ($value['type'] == 'timestamp') {

        } else {
            $out.='</tr>';
        }
    }
    $out.='</table>';
    $out.='<p class="submit">
    <input type="submit" class="button-primary" value="' . __('Save Changes') . '" />
</p>';
$out.='</form>';
return $out;
}

public function load_template($file) {
  $formfile=$this->themedir . $file . '.php';
    if (file_exists($formfile)) {
        ob_start();
        include ($this->themedir . $file . '.php');
        $html = ob_get_clean();
        return $html;
    } else {
        return $formfile .' '. _('not found');
    }
}

public function makefield($name, $type, $value, $default = false) {
    $value['value'] = get_option($name);
    switch ($type) {
        case 'text':
        $out.='<label for="' . $name . '">';
        $out.='<input id="' . $name . '" type="text" class="" name="' . $name . '" value="' . $value['value'] . '">';
        $out.='</label>';
        break;
        case 'image':
        $out.='<label for="' . $name . '">';
        $out.='<input id="' . $name . '" type="hidden" class="upload_image" name="' . $name . '" value="' . $value['value'] . '">';
        $out.='</label>';
        $out.='<br/><input id="' . $name . '_button" type="button" value="Upload Image" class="upload_image_button" />';
        break;
        case 'timestamp':
        $out.='<input type="hidden" name="' . $name . '" value="' . time() . '">';
        break;
        case 'page_id':
        $out.='<select name="' . $name . '" class="">';
        $args = array('posts_per_page' => 999,
            'post_type' => 'page'
            );
        if (!$this->pages) {
            $this->pages = get_posts($args);
        }
        $out.='<option value="0" ';
        if ($value['value'] == 0) {
            $out.='selected';
        }
        $out.='>' . __('Текущая') . '</option>';
        foreach ($this->pages as $page) {
            $out.='<option value="' . $page->ID . '" ';
            if ($value['value'] == $page->ID) {
                $out.='selected';
            }
            $out.='>';
            $out.=$page->post_title;
            $out.='</option>';
        }
        $out.='</select>';
        if (strlen($value['data_type']['short_code'])) {
//     $out.='</select>';
            $out.='<br/>Шорткод для страницы:<br/><textarea>[';
            $out.=$value['data_type']['short_code'];
            $out.=']</textarea>';
        }
        $out.='<br/>';
        if ($value['value'] > 0) {
            $out.='<a href="' . get_edit_post_link($value['value']) . '">Редактировать страницу</a>';
        }
        break;
        case 'hidden':
        break;
        default:
        break;
    }
    return $out;
}

public function init_ajax($key, $value) {
    add_action('wp_ajax_' . $key, $value);
    add_action('wp_ajax_nopriv_' . $key, $value);
}

public function csv_import($csvfile) {
    if (file_exists($csvfile)) {
        $file_handle = fopen($csvfile, 'r');
        $delimiter = ';';
        $enclosure = '"';
        $a = 0;
        while (!feof($file_handle)) {
            $line_of_text_ = fgetcsv($file_handle, 1024, $delimiter, $enclosure);
            if ($a == 0) {
                foreach ($line_of_text_ as $key => $value) {
                    if (strlen($value)) {
                        $header[$key] = mb_convert_encoding($value, mb_internal_encoding(), 'CP1251');
                    }
                }
            } else {
                if ($a > 0) {
                    foreach ($header as $key => $value) {
                        $line_of_text_temp[$value] = mb_convert_encoding($line_of_text_[$key], mb_internal_encoding(), 'CP1251');
                                //$line_of_text_temp[$value] = ($line_of_text_[$key]);
                    }
                    $line_of_text[] = $line_of_text_temp;
                }
            }
            $a++;
        }
        fclose($file_handle);
        return($line_of_text);
    } else {
        return false;
    }
}

public function menu($path = false) {
    if (!$path) {
        $path = $this->plugindir;
    }
    if (file_exists($path . '/options.php')) {
        add_menu_page(_('Options'), $this->pluginname, $this->settings['access'], $path . '/options.php');
        if ($this->registered_posts) {
            foreach ($this->registered_posts as $key => $value) {
                add_submenu_page("$path/options.php", $value['name'], $value['name'], $this->settings['access'], "edit.php?post_type=" . $key);
            }
        }
    }
}

public function init_cronevents() {
    $cron = $this->get_option('cron');
    if(!$cron){return;}
    foreach ($cron as $k => $event) {
        if (is_int($event['time']) && $event['time'] < time()) {
            $this->delete_option('cron', $k);
        }
        add_action($k, array($this, $event['function'][1]));
    }
}

        public function cron($time, $function, $params = false) { //Добавляем в расписание выполнение функции. $time - время, либо очередность, $function - наименование функции, $params - параметры, если нужно
            $this->dbg('cron');
            $this->dbg('Время ' . $time);
            $this->dbg('Функция:');
            $this->dbg($function);
            $this->dbg($params);
//// параметры для $params - array('params'=> то что передаем функции, 'single'=>одно событие, не планировать, если уже запланировано с такими же параметрами);
// add_action($this->prefix($actionname), $function);
            $actionname_ = array($function, $params);
            $actionname = $this->prefix . md5(serialize($actionname_));
            //add_action($actionname, $function);

            $this->update_option('cron', $actionname, array(
                'time' => $time,
                'function' => $function
                ));

            if (is_int($time)) {
                if ($params['single'] && wp_next_scheduled($actionname)) {
                    return;
                }
                if ($params['params']) {
                    wp_schedule_single_event($time, $actionname, $params['params']);
                } else {
                    wp_schedule_single_event($time, $actionname);
                }
            }
        }

        public function register_cpt($label = false, $settings = false) {
            $this->post_labels = array(
                'name' => __('%post%'),
                'singular_name' => __('%post%'),
                'add_new' => __('Add %post%'),
                'all_items' => __('All %post%'),
                'add_new_item' => __('Add %post%'),
                'edit_item' => __('Edit %post%'),
                'new_item' => __('New %post%'),
                'view_item' => __('View %post%'),
                'search_items' => __('Search %post%'),
                'not_found' => __('No %post%s found'),
                'not_found_in_trash' => __('No %post%s found in trash'),
                'parent_item_colon' => __('Parent %post%')
                );
            if (isset($label)) {
                foreach ($this->post_labels as $key => $value) {
                    $this->post_labels[$key] = ucwords(str_replace('%post%', $label, $value));
                }
            }
            $this->post_settings = array(
                'labels' => $this->post_labels,
                'public' => false,
                'show_ui' => true,
                'has_archive' => false,
                'show_in_menu' => false,
                'publicly_queryable' => false,
                'query_var' => true,
                'exclude_from_search' => true,
                'rewrite' => false,
                'capability_type' => 'post',
                'hierarchical' => false,
                'supports' => array(
                    'title',
                    'editor',
                    'revisions'
                    )
                );
            if (($settings)) {
                foreach ($settings as $key => $value) {
                    $this->post_settings[$key] = $value;
                }
            }
            //   (register_post_type(sanitize_title($this->post_labels['name']), $this->post_settings));
            $this->registered_posts[sanitize_title($this->post_labels['name'])] = $this->post_labels;
            $this->update_option('cpt', sanitize_title($this->post_labels['name']), $this->post_settings);
        }

        public function add_taxonomy_to_cpt($taxonomy, $posttype, $settings = false) {
            $tax = $this->get_option('taxonomies', $posttype);
            $labels = array(
                'name' => _x($taxonomy, 'taxonomy general name'),
                'singular_name' => _x($taxonomy, 'taxonomy singular name'),
                'search_items' => __('Search'),
                'all_items' => __('All items'),
                'parent_item' => __('Parent items'),
                'parent_item_colon' => __('Parent item:'),
                'edit_item' => __('Edit'),
                'update_item' => __('Update'),
                'add_new_item' => __('Add New'),
                'new_item_name' => __('New Name'),
                'menu_name' => __($taxonomy),
                );
            $args = array(
                'hierarchical' => true,
                'labels' => $labels,
                'show_ui' => true,
                /* 'query_var' => true, */
                /* 'rewrite' => array('slug' => sanitize_title($taxonomy)), */
                );
            if ($settings) {
                foreach ($settings as $key => $value) {
                    $args[$key] = $value;
                }
            }
            $tax[$posttype][$taxonomy] = $args;
            $this->update_option('taxonomies', $tax);
        }

        public function initscripts() {

            if (isset($_GET['page'])) {
                if ($_GET['page'] == $this->plugin_folder . '/options.php') {
                    add_action('admin_print_scripts', array($this, 'options_scripts'));
                    add_action('admin_print_styles', array($this, 'options_styles'));
                }
            }
        }

        public function add_metabox($type, $name, $args = false, $extra = false) {
            /* Создаем и выводим произвольные поля
             * (наименование поля, вид поля, запись, поле для все подобных записей (true) или только для этой
             */
            $type = $this->simplify($type);
            $default_args = array(
                'variant' => 'textarea',
                'pid' => false,
                'notsingle' => true,
                'cloneable' => false
                );

            if (is_array($args)) {
                foreach ($args as $key => $value) {
                    $default_args[$key] = $value;
                }
            }
            if ($default_args['notsingle']) {
                $key = $this->simplify($name) . $type;
                $this->update_option('custom_fields', $key, array(
                    'variant' => $default_args['variant'],
                    'post_type' => $type,
                    'notsingle' => $default_args['notsingle'],
                    'label' => $name,
                    'extra' => $extra
                    ));
            } else {
                $key = $this->simplify($name) . '__' . $default_args['pid'];
                $this->update_option('custom_fields', $key, array(
                    'variant' => $default_args['variant'],
                    'post_type' => $type,
                    'notsingle' => $default_args['notsingle'],
                    'post_id' => $default_args['pid'],
                    'label' => $name,
                    'extra' => $extra
                    ));
            }
            return get_post_meta($default_args['pid'], $key, true);
        }

        public function options_scripts() {
            wp_enqueue_script('media-upload');
            wp_enqueue_script('thickbox');

            wp_register_script($this->plugin_folder, WP_PLUGIN_URL . '/' . $this->plugin_folder . '/assets/scripts.js', array('jquery', 'media-upload', 'thickbox'));
            wp_enqueue_script($this->plugin_folder);
        }

        public function options_styles() {
            wp_enqueue_style($this->prefix . 'options', WP_PLUGIN_URL . '/' . $this->plugin_folder . '/style.css');
        }
        public function setlocale(){
            $this->lang=preg_replace('/\_[^\Z]*\Z/','',strtolower(get_locale()));
        }
    }
}


//add_action('muplugins_loaded', array('cw_fw', 'init'));
