<?php
/*
  Plugin Name: cweb custom feedback form
  Plugin URI: http://cweb24.ru
  Description: Allows to use custom form code. Just include file with your <form></form> with shortcode
  Author: Nikita Menshutin
  Version: 1.7
  Author URI: http://iwtech.ru
 */
defined('ABSPATH') or die("No script kiddies please!");
require dirname(__FILE__).'/cw_class.php';

Class cw_fb extends cw_fw16
{

    public function __construct($pluginname, $settings = '')
    {
        parent::__construct($pluginname, $settings);
        add_shortcode($this->prefix.'', array($this, 'printform'));
        add_action('wp_enqueue_scripts', array($this, 'loadscripts'));
        //$this->loadscripts();
        $this->init_ajax('cw_ff_processform', array($this, 'checkform'));
        $this->register_submissions();
        add_action('init',
            function() {
            $this->mails = apply_filters('cwfbmail');
        });
        add_action('wp_ajax_'.$this->prefix.'_hash', array($this, 'hash'));
        add_action('wp_ajax_nopriv_'.$this->prefix.'_hash', array($this, 'hash'));
    }

    public function checkform()
    {
        $data = ($_POST['data']['cw_ff[form']);
        if ($_POST['action'] != $this->prefix.'_processform') {
            echo 'error';
            die();
        }
        if (!$this->validate($data)) {
            echo 'invalid';
            die();
        }
        $names    = ($this->get_option($data['tech_name']));
        $formname = $names['formname'];
        unset($names['formname']);
        foreach ($names as $k => $v) {
            if (isset($data[$k])) {
                $values[$v] = $data[$k];
            }
        }
        $this->save_submission($formname, $values);
        echo 'success';
        die();
    }

    public function save_submission($name, $form)
    {
        $message = '';
        foreach ($form as $k => $v) {
            $message.=$k.' : '.$v."\n<hr/>\n";
        }
        $post_ = array(
            'post_type' => 'submission',
            'post_content' => $message,
            'post_title' => 'Form submission ['.$name.']',
            'post_status' => 'publish'
        );
        $id    = wp_insert_post($post_);
        //   $message.='<hr>--- <a href="http://cweb24.com">cweb24.com</a>';
        $this->send($id, $name, $message);
        if ($id) {
            wp_update_post(
                array('ID' => (int) $id,
                    'post_title' => 'Form submission #'.$id.' ['.$name.']',
                    'post_content' => $message."\n"._('Получатели:').' '
                    .implode(', ', $this->notify)
                )
            );
        }
        return $id;
    }

    public function send($id, $name, $message)
    {
        $headers                                 = 'From: '.get_bloginfo('name').' <noreaply@'.$_SERVER['HTTP_HOST'].'>'."\r\n";
        add_filter('wp_mail_content_type',
            create_function('', 'return "text/html;charset=utf-8";'));
        $message.=__('Отправлено с помощью').' <a href="http://cweb24.com">cweb24.com custom feedback forms</a>';
        $this->notify                            = array();
        $this->notify[get_option('admin_email')] = get_option('admin_email');
        if (!empty($this->mails)) {
            foreach ($this->mails as $mail) {
                $this->notify[$mail] = $mail;
            }
        }
        $subject = 'Обращение с сайта '.$_SERVER['HTTP_HOST'].' ['.$name.'] #'.$id;
        foreach ($this->notify as $email) {
            //cwlog($email);
            if (!wp_mail($email, $subject, $message, $headers)) {
                return false;
            }
        }
        return true;
    }

    public function validate($form)
    {
        $array_slice = array_slice($form, 0, 1);
        $ashift      = array_shift($array_slice);
        if ($this->makehash($ashift) != $form['protect2']) {
            if (current_user_can('manage_options')) {
                echo $ashift;
                echo "\n";
                echo $this->makehash($ashift);
                echo "\n";
                echo $form['protect2'];
                echo "\n";
                var_dump('Не совпадает hash');
            }
            return false;
        }
        $protect = $form['protect1'];
        $values_ = str_replace('\"', '&quot;', $form['values']);
        $values  = unserialize(str_replace('&quot;', '"', $values_));
        if (wp_verify_nonce($protect, $values_)) {
            return true;
        }
        return false;
    }

    public function printform($atts, $value)
    {
        if (!did_action('wp_print_scripts')) {
            return;
        }
        $form = '';
        $form = $this->load_template($value);
        $form = str_replace('</form>', $this->getUrl().'</form>', $form);
        preg_match_all('/name\=\"([^\"]*)\"/', $form, $names);
        preg_match_all('/<form[^name]*name\=\"([^\"]*)\"/', $form, $formname);
        if (isset($formname[1][0]) && strlen($formname[1][0])) {
            $this->formname = $formname[1][0];
        } else {
            $this->formname = $value;
        }
        //   $names['URL'] = $this->getUrl();
        foreach ($names[0] as $key => $value) {
            $testname = explode('_', $names[1][$key]);
            if (!isset($this->skip[$testname[0]])) {
                $form         = str_replace($value,
                    'name="'.$this->prefix.'[form]['.$key.']"', $form);
                $values[$key] = $names[1][$key];
            }
        }
        $append['values']     = $this->inputformat(serialize($values));
        $append['protect1']   = wp_create_nonce($append['values']);
        $append['tech_name']  = md5($this->formname);
        //   $append[$key]         = $this->getUrl();
        $names[1]['formname'] = $this->formname;
        $this->update_option($append['tech_name'], array());
        $this->update_option($append['tech_name'], $names[1]);
        $append_text          = '';
        foreach ($append as $key => $value) {
            $append_text .= '<input type="hidden" name="'.$this->prefix.'[form]['.$key.']" value="'.$value.'">';
        }
        $form = str_replace('</form>', $append_text.'</form>', $form);
        return '<div id="'.$this->prefix.$append['tech_name'].'" class="validateme '.$this->prefix.'">'.$form.'</div>';
    }

    public function loadscripts()
    {
        wp_enqueue_script('jq-validate-'.$this->lang,
            $this->plugin_uri.'/js/jquery.validate.min.js', array(), '1.3', true);
        wp_enqueue_script('jq-validate',
            $this->plugin_uri.'/js/local/messages_'.$this->lang.'.js', array(),
            '1.3', true);
        wp_enqueue_script('moment', $this->plugin_uri.'/js/moment.js', array(),
            '2.6.0', true);
        wp_enqueue_script('moment-'.$this->lang,
            $this->plugin_uri.'/js/'.$this->lang.'.js', array(), '2.6.0', true);
        /*        wp_enqueue_script('bootstrap-datetimepicker',
          $this->plugin_uri.'/js/bootstrap-datetimepicker.min.js', array(),
          '3.0.0', true);
          wp_enqueue_style('bootstrap-datetimepicker',
          $this->plugin_uri.'css/bootstrap-datetimepicker.min.css');
         * 
         */
        wp_enqueue_style($this->prefix.'css',
            $this->plugin_uri.'css/'.$this->prefix.'.css');
        wp_register_script('jquery-mask',
            $this->plugin_uri.'/js/jquery-maskedinput-1.3.min.js');
        wp_enqueue_script('jquery-mask',
            $this->plugin_uri.'/js/jquery-maskedinput-1.3.min.js', array(), 1.3,
            true);
        wp_register_script($this->prefix.'jqmd5',
            $this->plugin_uri.'/js/jquery.md5.js');
        wp_enqueue_script($this->prefix.'jqmd5',
            $this->plugin_uri.'/js/jquery.md5.js', array(), 1.0, true);
        wp_register_script($this->prefix.'val', $this->plugin_uri.'/js/cw_ff.js');
        wp_localize_script($this->prefix.'val', $this->prefix,
            array('ajax_url' => admin_url('admin-ajax.php')));
        wp_enqueue_script($this->prefix.'val', $this->plugin_uri.'/js/cw_ff.js',
            array(), 1.0, true);
    }

    public function register_submissions()
    {
        $labels = array(
            'name' => __('Submissions'),
            'singular_name' => __('Submission'),
            'add_new' => __('Add submission'),
            'all_items' => __('All submissions'),
            'add_new_item' => __('Add submission'),
            'edit_item' => __('Edit submission'),
            'new_item' => __('New submission'),
            'view_item' => __('View submission'),
            'search_items' => __('Search submission'),
            'not_found' => __('No submissions found'),
            'not_found_in_trash' => __('No submissions found in trash'),
            'parent_item_colon' => __('Parent submission')
        );
        $args   = array(
            'labels' => $labels,
            'public' => false,
            'show_ui' => true,
            'has_archive' => false,
            'show_in_menu' => true,
            'publicly_queryable' => false,
            'query_var' => true,
            'exclude_from_search' => true,
            'rewrite' => false,
            'capability_type' => 'post',
            'capabilities' => array(
            ),
            'hierarchical' => false,
            'supports' => array(
                'title',
                'editor'
            )
        );
        $this->register_cpt('submission', $args);
    }

    public function inputformat($value)
    {
        return str_replace('"', '&quot;', $value);
    }

    public function hash()
    {
        echo $this->makehash($_POST['data']);
        die();
    }

    public function getUrl($strip = true)
    {
        $before = '';
        $before = '<input type="hidden" name="'.
            _('URL').'" value="';

        return $before.'http'.(($_SERVER['SERVER_PORT'] == '443') ? 's' : '')
            .'://'.$_SERVER['SERVER_NAME'].($_SERVER['REQUEST_URI']).'">';
    }

    public function makehash($data)
    {
        $salted = $data.$this->prefix;
        return md5($salted);
    }
}
add_action('plugins_loaded', 'cwff_init');

function cwff_init()
{
    $fbform = new cw_fb('feedback form');
}
