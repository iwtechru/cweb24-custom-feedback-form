jQuery(document).ready(function () {
    ajax_url = (cw_ff['ajax_url']);
    jQuery.validator.setDefaults({
        submitHandler: function (form) {
            jQuery.fn.serializeObject = function ()
            {
                var o = {};
                var a = this.serializeArray();
                jQuery.each(a, function () {
                    if (o[this.name]) {
                        if (!o[this.name].push) {
                            o[this.name] = [o[this.name]];
                        }
                        o[this.name].push(this.value || '');
                    } else {
                        o[this.name] = this.value || '';
                    }
                });
                return o;
            };
            formarr = jQuery(form).serializeObject();
            datapf = (jQuery(form).attr('data-pf'));
            $form = jQuery('form[data-pf=' + datapf + ']');
            $form.addClass('cw_ff_submitting');
            cw_ff_processform(formarr);
        }
    });

    var ii = 0;
    jQuery('.validateme form').each(function () {
        $form = jQuery(this);
        $form.find('input[type=text],input[type=email],textarea,select').filter(':visible:first').addClass('firstfield');
        ii++;
        $form.attr('data-pf', ii);
        $form.append('<input data-ph="' + ii + '" class="protectme protectme" type="hidden" name="cw_ff[form][protect2]" value=""/>');
        cw_ff_protectform($form);
        $form.find('input:first').attr('data-pr', ii);
        $form.submit(function (e) {
            e.preventDefault();
        });

    });

    jQuery('.validateme form').each(function () {
        jQuery(this).validate();
    });
    jQuery('.validateme input[data-mask]').each(function () {
        $this = jQuery(this);
        $this.mask($this.attr('data-mask'));
    });
    jQuery('.validateme [type="tel"]').not('[data-mask]').keyup(function (key) {
        $this = jQuery(this);
        $this.val($this.val().replace(/[^\(\)\ \-0-9]/g, ''));
    }).change(function () {
        $this = jQuery(this);
        $this.val($this.val().replace(/[^0-9]/g, ''));
    });
    /* jQuery('.validateme [type="date"]').attr('type', 'text').addClass('datepick')
     jQuery('.datepick').datetimepicker({
     language: 'ru',
     pickTime: false,
     dateFormat: "DD/MM/YYYY",
     close: function (event, ui) {
     jQuery(this).dialog("destroy");
     }
     });
     jQuery('.datepick').blur(function () {
     $this = jQuery(this);
     if (is_date($this.val())) {
     } else {
     $this.val('');
     }
     }).change(function () {
     $this = jQuery(this);
     if (is_date($this.val())) {
     $this.blur();
     } else {
     $this.val('');
     }
     });
     jQuery('.datepick').click(function () {
     jQuery(this).datetimepicker('show');
     });
     jQuery('.validateme [type="time"]').datetimepicker({
     language: 'ru',
     pickDate: false,
     useStrict: true,
     pickTime: true
     });
     
     **/
    jQuery('input[data-pattern]').keyup(function () {
        $this = jQuery(this);
        $this.val($this.val().replace($this.attr('data-pattern')));
    });
    jQuery('.validateme form').each(function () {
        jQuery('input[pattern]').each(function () {
            $this = jQuery(this);
            pattern = $this.attr('pattern');
            $this.rules("add", {regx: pattern});
        });
    });
    function cw_ff_processform(form) {
        cw_ff_protectform(form, function () {
            $form = cw_ff_getform(form);
            form['cw_ff[form][protect2]'] = $form.find('.protectme').val();
            jQuery.ajax({
                url: ajax_url,
                type: 'post',
                data: {
                    action: 'cw_ff_processform',
                    data: form
                },
                success: function (msg) {
                    if (msg == 'success') {
                        if (typeof cwffcallback === 'function') {
                            cwffcallback();
                        } else {
                            jQuery('.cw_ff_submitting').closest('.validateme').find('.cw-ff-formreply').attr('style', 'display:block');
                            jQuery('.cw_ff_submitting').remove();
                        }
                    } else {

                        //alert('Произошла ошибка. Пожалуйста, попробуйте снова');

                    }
                    jQuery('.cw_ff_submitting').removeClass('cw_ff_submitting');

                }
            });
        });
    }

    function cw_ff_getform($form_) {
        formdivname = ($form_['cw_ff[form][tech_name]']);
        return jQuery('#cw_ff' + formdivname + ' form');
    }

    function cw_ff_protectform($form_, callback) {
        $form = cw_ff_getform($form_);
        $form.find('[type="submit"]').attr('disabled', true);
        val = $form.find('input,textarea,select:visible').eq(0).val();
        jQuery.ajax({
            url: ajax_url,
            type: 'post',
            data: {
                action: 'cw_ff_hash',
                data: val
            },
            success: function (msg) {
                $form.find('.protectme').val(msg);
                jQuery($form.find('[type="submit"]')).removeAttr('disabled');
                if (callback) {
                    setTimeout(callback(), 1000);
                }
            }
        });
    }
    function is_date(val) {
        var date_ = val.split('.');
        var date = new Date(date_[2], date_[1] - 1, date_[0]);
        day = date.getDate();
        if (day < 10) {
            day = '0' + day;
        }
        month = (parseInt(date.getMonth()) + 1);
        if (month < 10) {
            month = '0' + month;
        }
        var date2 = (day + '.' + month + '.' + date.getFullYear());
        if (date2 == val) {
            return true;
        } else {
            return false;
        }
    }
    jQuery.validator.addMethod(
            "regx",
            function (value, regexp) {
                var re = new RegExp(regexp);
                return re.test(value);
            },
            "Пожалуйста, введите правильное значение"
            );


});
jQuery.fn.serializeObject = function ()
{
    var o = {};
    var a = this.serializeArray();
    jQuery.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};
//jQuery(document).ready(function(){

//});
